package com.especializacao.itauspec1.cartoes.cliente.controllers;

import com.especializacao.itauspec1.cartoes.cliente.mappers.ClienteMapper;
import com.especializacao.itauspec1.cartoes.cliente.dtos.ClienteRequest;
import com.especializacao.itauspec1.cartoes.cliente.dtos.ClienteResponse;
import com.especializacao.itauspec1.cartoes.cliente.models.Cliente;
import com.especializacao.itauspec1.cartoes.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @PostMapping
    public ResponseEntity<ClienteResponse> registrar(@Valid @RequestBody ClienteRequest clienteRequest) {
      Cliente cliente = new Cliente(clienteRequest.getName());
      return new ResponseEntity<ClienteResponse>(ClienteMapper.toClienteResponse(clienteService.cadastrar(cliente)), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ClienteResponse GetById(@PathVariable(value = "id") int id) {
        return ClienteMapper.toClienteResponse(clienteService.buscar(id));
    }
}
