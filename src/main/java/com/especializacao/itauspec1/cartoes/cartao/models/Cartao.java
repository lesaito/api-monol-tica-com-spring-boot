package com.especializacao.itauspec1.cartoes.cartao.models;

import com.especializacao.itauspec1.cartoes.cliente.models.Cliente;

import javax.persistence.*;

@Entity
public class Cartao {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="cartao_id")
    private int id;

    @Column(nullable = false)
    private String numero;

    @ManyToOne
    @JoinColumn(name="user_id")
    private Cliente cliente;

    @Column(nullable = false)
    private Boolean ativo;

    public Cartao(String numero, Cliente cliente) {
        this.numero = numero;
        this.cliente = cliente;
    }

    public Cartao () {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
