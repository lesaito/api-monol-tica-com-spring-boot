package com.especializacao.itauspec1.cartoes.cartao.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CartaoRequest {
    private int clienteId;

    @NotBlank
    @Size(min = 3)
    private String numero;

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
